# Django messenger application
---

Welcome to Tynan Burke's WayUp coding challenge project. As requested, it is a real-time-ish person-to-person message center application, built with scaling in mind.

To run it locally, create a python 3 virtualenv, and run:

```
pip install -r requirements.txt
python manage.py migrate
python manage.py runserver
```

You may get a dependency conflict warning. I have not had time to resolve this. It does not affect the project's functionality.

You will also need to turn on a redis server at `redis://localhost:6379`. I'm not aware of any additional environment variables required for local development.

As all views require a login, you should create a superuser and go from there.

This application is deployed on Heroku at [https://fathomless-ocean-60181.herokuapp.com/](https://fathomless-ocean-60181.herokuapp.com/). There, it uses daphne to serve HTTP and WebSockets, backed by postgres (persisting users, messages, and associated django objects) and redis (managing workers/sockets).

The frontend is just Foundation and jQuery. Since I had a limited amount of time to work on this (I prioritized it, but life finds a way), I focused on architectural features that are best to get right from the beginning; a UI can be updated much more easily. I've only just reached the point where the javascript is getting out of hand, and now would be a good time to switch tracks and focus on moving it into React.

I chose Foundation because I've been meaning to get more practice in with it, and I rarely pass up an opportunity for low-stakes learning.

I selected django-channels for two reasons. One, it is an official part of the Django Project umbrella. Two, it was easy to find [instructions](https://blog.heroku.com/in_deep_with_django_channels_the_future_of_real_time_apps_in_django) for deploying it on Heroku. (While that's one major release behind, it was easy enough.) Since scalability is a key requirement in the product description, and I am the only developer, I thought it best to outsource load-balancing, database management, and the rest to a trusted third party.

If you follow that Heroku link, there are five users already created: `tynan`, `mathieu`, `jj`, `xiao`, and `damien`. Everybody's password (except mine) is `wayup_coding_challenge`.

What follows is a to-do/project management list.

### TODO
* Error handling!!
* Live tests
    * Not quite what we're looking for: https://channels.readthedocs.io/en/stable/tutorial/part_4.html
* Security
    * Investigate channels.auth.AuthMiddlewareStack
* CRUD
    * Mark messages as 'read' on click
    * Delete button
    * Required fields on form (now that it isn't using a request)
* Models
    * Replies/threads
    * Friends/contacts list or live search instead of select field
* UI
    * Message flashing instead of alerts
    * Do something better than `{{ form }}`
    * Generally make less awful
* Django
    * Richtext body field?
    * Static CDN (I like `django-storages`, currently using Heroku's `whitenoise` which... is functional)
* Misc.
    * SASS/SCSS pipeline (e.g. https://github.com/jrief/django-sass-processor)
    * Use string interpolation on js

### Known issues
* Dependency conflict (see pip)?
* Outbox doesn't receive pushes
* Javascript is getting out of hand; move to React
