import json

from channels.generic.websocket import AsyncWebsocketConsumer

from .models import Message


class MessageCenterConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.group_name = 'user_%s' % self.scope['user'].id
        await self.channel_layer.group_add(
                self.group_name, self.channel_name)
        await self.accept()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(
                self.group_name, self.channel_name)

    async def receive(self, text_data):
        msg_data = json.loads(text_data)
        new_msg = Message.messages.create_from_form_json(
                self.scope['user'], msg_data)
        recip_field = [f for f in msg_data if f['name']=='recipient'][0]
        recipient_id = recip_field['value']
        recipient_group_name = 'user_%s' % recipient_id
        await self.channel_layer.group_send(
                recipient_group_name,
                {
                        'type': 'new_message',
                        'message': new_msg.to_json()
                }
        )

    async def new_message(self, event):
        await self.send(text_data=json.dumps(event))
