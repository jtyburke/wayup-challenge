from django import forms
from django.contrib.auth.models import User

from .models import Message


def _get_user_choices():
    all_users = User.objects.all()
    return [(u.id, u.username) for u in all_users]


class BaseMessageForm(forms.Form):
    subject = forms.CharField(max_length=255, required=True,
            initial=Message.SUBJECT_DEFAULT)
    body = forms.CharField(required=True, widget=forms.Textarea)


class MessageForm(BaseMessageForm):
    '''A form for creating a new Message, sent by the current user,
    to the selected user.'''
    recipient = forms.ChoiceField(choices=_get_user_choices, 
            required=True)


class ReplyForm(BaseMessageForm):
    '''A form specifically for sending a Message from the conversations
    view'''
    pass
