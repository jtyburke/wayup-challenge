# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json

from django.contrib.auth.models import User
from django.db import models
from django.db.models import Q


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class MessageQuerySet(models.QuerySet):
    def between(self, user1, user2, order='ascending'):
        order_by = 'created_at' if order=='ascending' else '-created_at'
        q = Q(sender=user1, recipient=user2) | Q(sender=user2, recipient=user1)
        return self.filter(q).order_by(order_by)

    def sent_by(self, user):
        return self.filter(sender=user).order_by('-created_at')

    def sent_to(self, user):
        return self.filter(recipient=user).order_by('-created_at')


class MessageManager(models.Manager):
    def get_queryset(self):
        return MessageQuerySet(self.model, using=self._db)

    def between(self, user1, user2):
        return self.get_queryset().between(user1, user2)

    def sent_by(self, user):
        return self.get_queryset().sent_by(user)

    def sent_to(self, user):
        return self.get_queryset().sent_to(user)

    def create_from_form(self, request, form):
        new_msg = Message(
                sender=request.user, 
                recipient=User.objects.get(pk=form.data['recipient']),
                subject=form.data['subject'],
                body=form.data['body'],
        )
        new_msg.save()
        return new_msg

    def create_from_form_json(self, user, form_data):
        get_field = lambda s: [f for f in form_data if f['name']==s][0]
        new_msg = Message(
                sender=user,
                recipient=User.objects.get(
                        pk=get_field('recipient')['value']
                ),
                subject=get_field('subject')['value'],
                body=get_field('body')['value'],
        )
        new_msg.save()
        return new_msg


class Message(BaseModel):
    '''A message sent between two users'''
    DATE_FMT = '%m/%d/%y %H:%M:%S'
    SUBJECT_DEFAULT = "[no subject]"

    body = models.TextField(blank=False)
    recipient = models.ForeignKey(User, related_name='received_messages',
            blank=False, on_delete=models.CASCADE)
    sender = models.ForeignKey(User, related_name='sent_messages', 
            blank=False, on_delete=models.CASCADE)
    subject = models.CharField(max_length=255, blank=True, 
            default=SUBJECT_DEFAULT)
    unread = models.BooleanField(default=True)

    messages = MessageManager()

    def __str__(self):
        return "%s to %s: %s (%s)" % (self.sender, self.recipient,
                self.subject, self.created_at.strftime(self.DATE_FMT))

    def to_json(self):
        user_to_dict = lambda u: {'id': u.id, 'name': u.username}
        # should really be part of User, but using default user
        return json.dumps({
                'id': self.id,
                'sender': user_to_dict(self.sender),
                'recipient': user_to_dict(self.recipient),
                'subject': self.subject,
                'body': self.body,
                'unread': self.unread,
                'created_at': self.created_at.strftime(self.DATE_FMT),
        })
