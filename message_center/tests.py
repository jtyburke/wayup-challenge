# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.test import TestCase

from .models import Message


class MessageTestCase(TestCase):
    def setUp(self):
        super(MessageTestCase, self).setUp()
        self.user1 = User.objects.create_user(
                username='user1', password='abc123')
        self.user2 = User.objects.create_user(
                username='user2', password='abc123')

    def test_empty_queries(self):
        self.assertEqual(len(Message.messages.sent_by(self.user1)), 0)
        self.assertEqual(len(Message.messages.sent_by(self.user2)), 0)
        self.assertEqual(len(Message.messages.sent_to(self.user1)), 0)
        self.assertEqual(len(Message.messages.sent_to(self.user2)), 0)

    def test_queries(self):
        m1 = Message.messages.create(
                sender=self.user1, recipient=self.user2, body="body!")
        m2 = Message.messages.create(
                sender=self.user2, recipient=self.user1, body="body!")
        self.assertEqual(len(Message.messages.sent_by(self.user1)), 1)
        self.assertEqual(len(Message.messages.sent_by(self.user2)), 1)
        self.assertEqual(len(Message.messages.sent_to(self.user1)), 1)
        self.assertEqual(len(Message.messages.sent_to(self.user2)), 1)
        self.assertEqual(Message.messages.sent_by(self.user1).first(), m1)
        self.assertEqual(Message.messages.sent_by(self.user2).first(), m2)
        self.assertEqual(Message.messages.sent_to(self.user1).first(), m2)
        self.assertEqual(Message.messages.sent_to(self.user2).first(), m1)
