from django.conf.urls import url

from . import consumers, views


urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^conversation/(\d+)/', views.ConversationView.as_view(),
            name='conversation'),
]
