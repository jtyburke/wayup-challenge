# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
import logging
import sys

from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect, render
from django.views.generic.base import View

from .forms import MessageForm, ReplyForm
from .models import Message


class IndexView(LoginRequiredMixin, View):
    template_name = 'message_center/index.html'
    form_class = MessageForm

    def _get_shared_context(self, request):
        msgs = Message.messages.sent_to(request.user)
        sent_messages = Message.messages.sent_by(request.user)
        all_msgs_json = ([m.to_json() for m in msgs] 
                + [m.to_json() for m in sent_messages]
        )
        return {
                'my_messages': msgs,
                'sent_messages': sent_messages,
                'json_messages': all_msgs_json,
        }

    def get(self, request):
        msgs = Message.messages.sent_to(request.user)
        context = self._get_shared_context(request)
        context['form'] = self.form_class()
        context['sent_messages'] = Message.messages.sent_by(request.user)
        return render(request, self.template_name, context)

    def post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid():
            try:
                Message.messages.create_from_form(request, form)
                # TODO flash success
                return redirect('index')
            except:
                err = sys.exc_info()[0]
                logging.error(err)
                # TODO flash error
        context = self._get_shared_context(request)
        context['form'] = form
        return render(request, self.template_name, context)


class ConversationView(LoginRequiredMixin, View):
    template_name = 'message_center/conversation.html'
    form_class = ReplyForm

    def _get_shared_context(self, request, user_id):
        other_user = User.objects.get(pk=user_id)
        return {
                'msgs': Message.messages.between(request.user, other_user),
                'other_user': other_user,
                'form': self.form_class(),
        }


    def get(self, request, user_id):
        other_user = User.objects.get(pk=user_id)
        context = self._get_shared_context(request, user_id)
        return render(request, self.template_name, context)

    def post(self, request, user_id):
        form = self.form_class(request.POST)
        if form.is_valid():
            try:
                Message.messages.create(
                        sender=request.user,
                        recipient=User.objects.get(pk=user_id),
                        subject=form.data['subject'],
                        body=form.data['body'],
                )
                # TODO flash success
            except:
                err = sys.exc_info()[0]
                logging.error(err)
                # TODO flash error
        context = self._get_shared_context(request, user_id)
        context['form'] = form
        return render(request, self.template_name, context)
