import os
import django
from channels.routing import get_default_application

'''Settings/constructor for async socket layer'''

os.environ.setdefault("DJANGO_SETTINGS_MODULE",
        "wayup_challenge.settings.production")
django.setup()

application = get_default_application()
