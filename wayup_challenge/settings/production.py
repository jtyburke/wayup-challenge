from __future__ import unicode_literals

import os

from .base import *

DEBUG = False
SECRET_KEY = os.environ.get('WAYUP_CHALLENGE_SECRET_KEY')

import dj_database_url
DATABASES['default'] =  dj_database_url.config()
